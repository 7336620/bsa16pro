var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Project = new Schema({
	users:{
		userList:[{
			userId :  Schema.User.ObjectId,
			userName: String
		}],
		required: true
	},
	technologies:{
		technologiesList:[{
			technologyId :  Schema.Technology.ObjectId,
			technologyName: String
		}],
		required: true
	},
	name{
		projectName: String,
		required: true
	},
	description{
		type: String
	},
	additionalData{
		//  for discuss
	},
	timeBegin{
		type: Date,
		required: true
	},
	timeEnd{
		type: Date,
		required: true
	},
	tags{
		tagsList:[{
			tagId :  Schema.Tags.ObjectId,
			tagName: String
		}]
	},
	stage{
		type: String  
	}
});

var User = new Schema({
	userId: ObjectId,
    userName: String,
    userPW: String, // may be hashed
	avatar: Buffer
});


var Technology = new Schema({
	technologyId: ObjectId,
    technologyName: String,
	specialImage: Buffer
});

var Tags = new Schema({
	tagId: ObjectId,
    tagName: String,
	specialImage: Buffer
});

var RequestedProject = new Schema({
	name{
		projectName: String,
		required: true
	},
	description{
		type: String
	},
	tags{
		tagsList:[{
			tagId :  Schema.Tags.ObjectId,
			tagName: String
		}]
	},
	technologies:{
		technologiesList:[{
			technologyId :  Schema.Technology.ObjectId,
			technologyName: String
		}],
		required: true
	},
	coditions{
		type: String  
	},
	additionalData{
		//  for discuss
	}
});